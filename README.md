# Visualizer #

Visualizer is a pulseaudio-based audio stream visualizer for Pulseaudio, written in C.

### Build Instructions ###

Just run:

    make

### Dependencies ###

Visualizer needs the following to run:

- Pulseaudio
- pactl
- pacat
- Allegro 5.0 (+ Allegro\_primitives)

### Choosing streams ###

Unfortunately, visualizer does not currently support specifying which stream it monitors, however,
the stream being monitored can be changed during the execution of the program via 
[pavucontrol](https://freedesktop.org/software/pulseaudio/pavucontrol/), which usually comes bundled
with pulseaudio. Streams can be input streams (such as microphones), or monitors of output streams.

**Note: Visualizer will default to the first running stream, if no running streams are found at startup, 
it will immediately exit**
